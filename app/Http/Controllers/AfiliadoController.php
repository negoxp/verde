<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AfiliadosRequest;

use App\Afiliados;
use App\Secciones;
use Image; 
use Excel; 
use Auth;

class AfiliadoController extends Controller {

	
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$afiliados = Afiliados::all();
        return view('afiliado.index')->with(compact(['afiliados']));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$title="Nuevo";
        $record=false;
        $action = 'store';
        return view('afiliado.form')->with(compact(['title','restaurants','record','action'])); 
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AfiliadosRequest $request)
	{
		//

        $afiliado = new Afiliados();
        $this->fields($afiliado,$request);
        $afiliado->save();

        if ($request->file('foto')){

	        $imageName = $afiliado->id . '.' .$request->file('foto')->getClientOriginalExtension();

	        $imageNamer =strtolower($afiliado->id . '.jpg');

		    //$imageNamer =$afiliado->id . '.jpg';

		    $request->file('foto')->move(
		        base_path() . '/public/images/fotos/', $imageName
		    );

		    $path = url() . '/images/fotos/'.$imageName;

		    $image = Image::make($path)->encode('jpg', 100)->save(base_path() . '/public/images/fotos/'.$imageNamer);

		    $image = Image::make($path)->resize(150, 150)->encode('jpg', 100)->save(base_path() . '/public/images/fotos/150/'.$imageNamer);


		    $afiliado->foto = $imageName;
		    $afiliado->save();
		}

		return redirect("afiliados")->with('success','Afiliado created successfully');

        #return redirect("afiliados/{$afiliado->id}/edit")->with('success','Afiliado created successfully');
	}

		public function secciones(Request $request)
    {

        $s = $request->input('s');

        $seccion = Secciones::where('seccion','=',$s)->take(1)->first();

				$r = ['seccion' => $s, 'dl' => $seccion->dl];

        return response()->json($r);

    }


	public function exportxls()
	{
		$afiliados = Afiliados::select()->get();
		
		Excel::create('afiliados', function($excel) use($afiliados) {
		    $excel->sheet('Afiliados', function($sheet) use($afiliados) {
		        $sheet->fromArray($afiliados);
		    });
		})->export('xls');
		
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	
		$title="Editar";
        $record = Afiliados::find($id);
        $action = 'update';
        return view('afiliado.form')->with(compact(['title','record','action']));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($id, AfiliadosRequest $request)
	{
		//
        $afiliado = Afiliados::find($id);
        $this->fields($afiliado,$request);
        $afiliado->save();
        if ($request->file('foto')){
		    $imageName = $afiliado->id . '.' .$request->file('foto')->getClientOriginalExtension();	

		    $imageName =strtolower($imageName);

		    $imageNamer =strtolower($afiliado->id . '.jpg');

		    //$imageNamer =$afiliado->id . '.jpg';

		    $request->file('foto')->move(
		        base_path() . '/public/images/fotos/', $imageName
		    );

		    $path = url() . '/images/fotos/'.$imageName;

		    $image = Image::make($path)->encode('jpg', 100)->save(base_path() . '/public/images/fotos/'.$imageNamer);

		    $image = Image::make($path)->resize(150, 150)->encode('jpg', 100)->save(base_path() . '/public/images/fotos/150/'.$imageNamer);

		    $afiliado->foto = $imageName;
		    $afiliado->save();
	    }

	    return redirect("afiliados")->with('success','Afiliado updated successfully');

        //return redirect("afiliados/{$afiliado->id}/edit")->with('success','Afiliado updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$afiliado = Afiliados::find($id);
        $afiliado->delete();
        return redirect("afiliados")->with('success','Afiliado eliminado successfully');
	}

	private function fields(&$afiliado,$request){
        $afiliado->nombres = $request->nombres;
        $afiliado->paterno = $request->paterno;
        $afiliado->materno = $request->materno;
        $afiliado->calle = $request->calle;
        $afiliado->numero = $request->numero;
        $afiliado->interior = $request->interior;
        $afiliado->colonia = $request->colonia;
        $afiliado->localidad = $request->localidad;
        $afiliado->municipio = $request->municipio;
        $afiliado->clave_electoral = $request->clave_electoral;
        $afiliado->seccion = $request->seccion;
        $afiliado->distrito = $request->distrito;
        $afiliado->celular = $request->celular;
        $afiliado->telefono = $request->telefono;
        $afiliado->correo = $request->correo;
        $afiliado->cp = $request->cp;
        if ($request->cargo==""){
        	$afiliado->cargo = "SIMPATIZANTE";
        }else{
        	$afiliado->cargo = $request->cargo;
        }
        $afiliado->estado = 'Colima';
        $afiliado->user_id = Auth::id();

        //$afiliado->foto = $request->foto;

    }

}
