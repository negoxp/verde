<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AfiliadosRequest;

use App\Afiliados;
use Image; 
use Excel; 
use Auth;
use DB;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$afiliados = Afiliados::all();
		$afiliados = DB::table('afiliados')
                     ->select(DB::raw('count(1) as total, municipio'))
                     ->groupBy('municipio')
                     ->get();

		return view('home')->with(compact(['afiliados']));
	} 

}
