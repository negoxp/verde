<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAfiliadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('afiliados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombres', 255);
            $table->string('paterno', 255);
            $table->string('materno', 255);

            $table->string('calle', 255);
            $table->string('numero', 50);
            $table->string('interior', 50);

            $table->string('colonia', 255);
            $table->string('municipio', 255);
            $table->string('estado', 255);

            $table->string('clave_electoral', 255);
            $table->string('seccion', 255);
            $table->string('distrito', 255);

            $table->string('celular', 255);
            $table->string('telefono', 255);

            $table->string('correo', 255);
            $table->string('facebook', 255);
            $table->string('twitter', 255);

            $table->boolean('is_processed')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('afiliados');
	}

}
