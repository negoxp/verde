<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFotoToAfiliadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('afiliados', function(Blueprint $table)
		{
			$table->string('foto', 255);
			$table->string('cargo', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('afiliados', function(Blueprint $table)
		{
			$table->dropColumn('foto');
			$table->dropColumn('cargo');
		});
	}

}
