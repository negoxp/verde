<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCpToAfiliados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('afiliados', function(Blueprint $table)
		{
			$table->string('cp', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('afiliados', function(Blueprint $table)
		{
			$table->dropColumn('cp');
		});
	}

}
