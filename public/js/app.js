$(document).ready(function(){

            $('.mydatatable').dataTable( {
              "bFilter": false,
              "bPaginate": true,
              "bLengthChange": false,
            } );

  $(".txtQty").numeric()
  $(".isnumber").numeric()
  $(".iscent").numeric({ decimal : false })
  $(".visaMastercardPercentage").prop('disabled', true)

  $('.claveelectoral').mask('LLLLLLNNNNNNXXXXXX', {'translation': {
                                        L: {pattern: /[A-Za-z]/}, 
                                        X: {pattern: /[A-Za-z0-9]/}, 
                                        N: {pattern: /[0-9]/}
                                      }
                                });

  $('.phone').mask('(000) 000-0000', {placeholder: "(___)___-____"});


  function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
            $('#blah').show();
        }

        reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function(){
      readURL(this);
  });

  $('.mayusculas').keyup(function() {
        this.value = this.value.toUpperCase();
    });

});