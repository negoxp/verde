@extends('app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}} Afiliado</div>

				<div class="panel-body">
					





					{!! Form::open(array('route' => ['afiliados.destroy', $record ? $record->id : 0], 'method' => 'delete', 'onsubmit'=>'return confirm("Are you sure you want to delete?")')) !!}
					<div class="pull-right btn-group">
					        <a href="{{url('afiliados')}}" class="btn btn-default btn-large">Regresar <span class="icon   icon-arrow-thin-left"></span></a>
					        @if($record)<button type="submit" class="btn btn-danger btn-large">Eliminar</button>@endif
					        <button type="button" onclick="enviar_form();" class="btn btn-success btn-large">Guardar</button>

					</div>
					{!! Form::close() !!}
					<br/>

					{!! Form::open(array('route' => ['afiliados.' . $action , $record ? $record->id : ''], 'method' => $record ? 'PUT' : 'POST','id'=>'main',  'files'=>true)) !!}

					

					<div class="row">
						<div class="col-md-3">

							<br/>

							<h2>Foto</h2>
	                
	                    	@if ($record)
	                    		<img id="blah" src="{{ asset('images/fotos/'.$record->id.'.jpg') }}" class="img-responsive" style="position: relative; top: -bs-example-navbar-collapse-15px; width:200px;">
	                    	@else
	                    		<img id="blah" src="#" class="img-responsive" style="position: relative; top: -bs-example-navbar-collapse-15px; width:200px; display:none;">
	                    	@endif

	                    	<input type="file" name="foto" multiple accept='image/*' id="imgInp" >

			                @if ($errors->has('foto'))
		                        <div class="alert alert-danger">
		                            <strong>{{ $errors->first('foto') }}</strong>
		                        </div>
		                    @endif



						</div>
						<div class="col-md-9">

							<h2>Nombre</h2>
			                <div class="row">
			                    
			                    <div class="col-md-4">
			                        <input type="text" class="form-control mayusculas" name="paterno" value="{{$record ? $record->paterno : ''}}">
			                        @if ($errors->has('paterno'))
			                            <div class="alert alert-danger">
			                                <strong>{{ $errors->first('paterno') }}</strong>
			                            </div>
			                        @endif
			                    </div>
			                    <div class="col-md-4">
			                        <input type="text" class="form-control mayusculas" name="materno" value="{{$record ? $record->materno : ''}}">
			                        @if ($errors->has('materno'))
			                            <div class="alert alert-danger">
			                                <strong>{{ $errors->first('materno') }}</strong>
			                            </div>
			                        @endif
			                    </div>
			                    <div class="col-md-4">
			                        <input type="text" class="form-control mayusculas" name="nombres" value="{{$record ? $record->nombres : ''}}">
			                        @if ($errors->has('nombres'))
			                            <div class="alert alert-danger">
			                                <strong>{{ $errors->first('nombres') }}</strong>
			                            </div>
			                        @endif
			                    </div>
		                	</div>
		                	<div class="row">
			                    <div class="col-md-4">Paterno</div>
			                    <div class="col-md-4">Materno</div>
			                    <div class="col-md-4">Nombre(s)</div>
			                </div>

			        <br/>
			        <h2>Dirección</h2>
	                <div class="row">
	                	<div class="col-md-8">
	                        <input type="text" class="form-control" name="calle" value="{{$record ? $record->calle : ''}}">
	                        @if ($errors->has('calle'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('calle') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-2">
	                        <input type="text" class="form-control iscent" name="numero" value="{{$record ? $record->numero : ''}}">
	                        @if ($errors->has('numero'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('numero') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-2">
	                        <input type="text" class="form-control" name="interior" value="{{$record ? $record->interior : ''}}">
	                        @if ($errors->has('interior'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('interior') }}</strong>
	                            </div>
	                        @endif
	                    </div>
                	</div>

                	<div class="row">
	                    <div class="col-md-8">Calle</div>
	                    <div class="col-md-2">Número</div>
	                    <div class="col-md-2">Interior</div>
	                </div>

	               	<br/>

	               	<div class="row">
	                	<div class="col-md-3">
	                        <input type="text" class="form-control" name="colonia" value="{{$record ? $record->colonia : ''}}">
	                        @if ($errors->has('colonia'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('colonia') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-2">
	                        <input type="text" class="form-control" name="cp" value="{{$record ? $record->cp : ''}}">
	                        @if ($errors->has('cp'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('cp') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-3">
	                        <input type="text" class="form-control" name="localidad" value="{{$record ? $record->localidad : ''}}">
	                        @if ($errors->has('colonia'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('localidad') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-4">
	                        <select class="form-control" name="municipio">
	                            <option disabled>Municipio</option>
	                            <option {{$record && $record->municipio == 'Armeria' ? 'selected' : ''}}>Armeria</option>
	                            <option {{$record && $record->municipio == 'Colima' ? 'selected' : ''}}>Colima</option>
	                            <option {{$record && $record->municipio == 'Comala' ? 'selected' : ''}}>Comala</option>
	                            <option {{$record && $record->municipio == 'Coquimatlan' ? 'selected' : ''}}>Coquimatlan</option>
	                            <option {{$record && $record->municipio == 'Cuauhtemoc' ? 'selected' : ''}}>Cuauhtemoc</option>
	                            <option {{$record && $record->municipio == 'Ixtlahuacan' ? 'selected' : ''}}>Ixtlahuacan</option>
	                            <option {{$record && $record->municipio == 'Manzanillo' ? 'selected' : ''}}>Manzanillo</option>
	                            <option {{$record && $record->municipio == 'Minatitlan' ? 'selected' : ''}}>Minatitlan</option>
	                            <option {{$record && $record->municipio == 'Tecoman' ? 'selected' : ''}}>Tecoman</option>
	                            <option {{$record && $record->municipio == 'Villa de Alvarez' ? 'selected' : ''}}>Villa de Alvarez</option>
	                        </select>
	                        @if ($errors->has('municipio'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('municipio') }}</strong>
	                            </div>
	                        @endif
	                    </div>
                	</div>

                	<div class="row">
	                    <div class="col-md-3">Colonia</div>
	                    <div class="col-md-2">C.P.</div>
	                    <div class="col-md-3">Localidad</div>
	                    <div class="col-md-4">Municipio</div>
	                </div>


                		</div>
                	</div>

                	



	                

	                <br/>
	                <div class="row">
	                	<div class="col-md-4">
	                        <input type="text" class="form-control claveelectoral" id="clave_electoral" name="clave_electoral" value="{{$record ? $record->clave_electoral : ''}}">
	                        <input type="text" class="form-control claveelectoral" id="clave_electoral2" name="clave_electoral2" value="{{$record ? $record->clave_electoral : ''}}">
	                        @if ($errors->has('clave_electoral'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('clave_electoral') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-4">
	                    		
	                    		<select class="form-control" name="distrito" id="distrito">
	                            <option disabled>DISTRITO</option>
	                            <option {{$record && $record->distrito == 'I' ? 'selected' : ''}}>I</option>
	                            <option {{$record && $record->distrito == 'II' ? 'selected' : ''}}>II</option>
	                            <option {{$record && $record->distrito == 'III' ? 'selected' : ''}}>III</option>
	                            <option {{$record && $record->distrito == 'IV' ? 'selected' : ''}}>IV</option>
	                            <option {{$record && $record->distrito == 'V' ? 'selected' : ''}}>V</option>
	                            <option {{$record && $record->distrito == 'VI' ? 'selected' : ''}}>VI</option>
	                            <option {{$record && $record->distrito == 'VII' ? 'selected' : ''}}>VII</option>
	                            <option {{$record && $record->distrito == 'VIII' ? 'selected' : ''}}>VIII</option>
	                            <option {{$record && $record->distrito == 'IX' ? 'selected' : ''}}>IX</option>
	                            <option {{$record && $record->distrito == 'X' ? 'selected' : ''}}>X</option>
	                            <option {{$record && $record->distrito == 'XI' ? 'selected' : ''}}>XI</option>
	                            <option {{$record && $record->distrito == 'XII' ? 'selected' : ''}}>XII</option>
	                            <option {{$record && $record->distrito == 'XIII' ? 'selected' : ''}}>XIII</option>
	                            <option {{$record && $record->distrito == 'XIV' ? 'selected' : ''}}>XIV</option>
	                            <option {{$record && $record->distrito == 'XV' ? 'selected' : ''}}>XV</option>
	                            <option {{$record && $record->distrito == 'XVI' ? 'selected' : ''}}>XVI</option>
	                        </select>
	                        @if ($errors->has('distrito'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('distrito') }}</strong>
	                            </div>
	                        @endif



	                    </div>
	                    <div class="col-md-4">
	                    	<input type="text" id="seccion" class="form-control iscent" name="seccion" value="{{$record ? $record->seccion : ''}}">
	                        @if ($errors->has('seccion'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('seccion') }}</strong>
	                            </div>
	                        @endif
	                    </div>
                	</div>

                	<div class="row">
	                    <div class="col-md-4">Clave Electoral</div>
	                    <div class="col-md-4">Distrito</div>
	                    <div class="col-md-4">Sección</div>
	                </div>

	                <h2>Contacto</h2>
	                <br/>
	                <div class="row">
	                	<div class="col-md-4">
	                        <input type="text" class="form-control phone" name="celular" value="{{$record ? $record->celular : ''}}">
	                        @if ($errors->has('celular'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('celular') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-4">
	                    	<input type="text" class="form-control phone" name="telefono" value="{{$record ? $record->telefono : ''}}">
	                        @if ($errors->has('telefono'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('telefono') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-4">
	                    	<input type="email" class="form-control" name="correo" value="{{$record ? $record->correo : ''}}">
	                        @if ($errors->has('correo'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('correo') }}</strong>
	                            </div>
	                        @endif
	                    </div>
                	</div>

                	<div class="row">
	                    <div class="col-md-4">Celular</div>
	                    <div class="col-md-4">Telefono</div>
	                    <div class="col-md-4">Correo</div> 
	                </div>

	                <br/>
	                <div class="row">
	                	<div class="col-md-4">
	                		<select class="form-control" name="pcargo" id="pcargo">
	                			<option {{$record && $record->cargo == 'SIMPATIZANTE' ? 'selected' : ''}}>SIMPATIZANTE</option>
	                			<option {{$record && $record->cargo == 'MILITANTE' ? 'selected' : ''}}>MILITANTE</option>
	                			<option {{$record ? ($record->cargo != 'SIMPATIZANTE' && $record->cargo != 'MILITANTE') ? 'selected' : '' : ''}}>OTRO</option>
	                		</select>
	                	</div>
	                	<div class="col-md-4">
	                        <input type="text" class="form-control cargo" name="cargo" value="{{$record ? $record->cargo!="" ? $record->cargo :  'SIMPATIZANTE'  : 'SIMPATIZANTE'}}" style="{{$record ? $record->cargo==="SIMPATIZANTE" || $record->cargo==="MILITANTE" ? 'display:none;' :  ''  : 'display:none;'}}">
	                        @if ($errors->has('cargo'))
	                            <div class="alert alert-danger">
	                                <strong>{{ $errors->first('cargo') }}</strong>
	                            </div>
	                        @endif
	                    </div>
	                    <div class="col-md-4">
	                    </div>
                	</div>

                	<div class="row">
	                    <div class="col-md-4">Cargo</div>
	                    <div class="col-md-4"></div>
	                    <div class="col-md-4"></div>
	                </div>

	               

	                


					{!! Form::close() !!}


					{!! Form::open(array('route' => ['afiliados.destroy', $record ? $record->id : 0], 'method' => 'delete', 'onsubmit'=>'return confirm("Are you sure you want to delete?")')) !!}
					<div class="pull-right btn-group">
					        <a href="{{url('afiliados')}}" class="btn btn-default btn-large">Regresar <span class="icon   icon-arrow-thin-left"></span></a>
					        @if($record)<button type="submit" class="btn btn-danger btn-large">Eliminar</button>@endif
					        <button type="button" onclick="enviar_form();" class="btn btn-success btn-large">Guardar</button>

					</div>
					{!! Form::close() !!}
					<br/>




				</div>
			</div>
		</div>
	</div>
</div>


@endsection
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
 	function enviar_form(){
  	var password = $("#clave_electoral").val();
  	var password2 = $("#clave_electoral2").val();
  	
  	if (password.length){
  		if (password==password2){
  			$('#main').submit();
  		}else{
  			alert("La claves electorales son distintas");
				$("#clave_electoral").focus();
  		}
  	}else{
  		alert("La clave electoral esta vacia");
  		$("#clave_electoral").focus();
  	}
  }

	$(function() {
  	$( '#pcargo' ).change(function() {
  		//alert( "Handler for .change() called." );
  		$('.cargo').val(this.value); 
			if (this.value=="OTRO"){
				$('.cargo').val(""); 
				$('.cargo').show();
			}else{
				$('.cargo').hide();
			}
		});

 

  	$("#seccion").focusout(function(){
    	var seccion = $(this).val();
			$.get( "<?php echo e(url('afiliados/secciones?s=')); ?>"+seccion).done(function( data ) {
    		var value =data["dl"].toString();
    		$("#distrito option").each(function()
					{
					    var optvalue =$(this).val();
					    if (optvalue==value){
					    	$(this).attr("selected","selected");
					    }
					});
  		});
		});


	});

</script>
