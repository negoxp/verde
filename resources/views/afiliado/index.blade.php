@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Afiliados</div>

				<div class="panel-body">
					<div class="pull-right"><a href="{{url('afiliados/create')}}" class="btn btn-success">Agregar <span class="icon  icon-plus"></span></a></div>
				

					<div class="pull-left"><a href="{{url('afiliados/exportxls')}}" class="btn btn-info">Exportar xls <span class="icon  icon-plus"></span></a></div>
				

					<table class="table" id="dynamic-table">
					    <thead>
					        <tr>
					            <th>Folio</th>
					            <th>Foto</th>
					            <th>Nombre</th>
					            <th>Municipio</th>
					            <th>Celular</th>
					            <th>Cargo</th>
					            <th width="1%"></th>
					        </tr>
					    </thead>
					    <tbody>
					        @foreach($afiliados as $a)
					        <tr>
					            <td>{{$a->id}}</td>
					            <td><img src="{{ asset('images/fotos/'.$a->id.'.jpg') }}" class="img-responsive" style="position: relative; top: -bs-example-navbar-collapse-15px; width:50px; height:50px;"></td>
					            <td>{{$a->nombres}} {{$a->paterno}} {{$a->materno}}</td>
					            <td>{{$a->municipio}}</td>
					            <td>{{$a->celular}}</td>
					            <td>{{$a->cargo}}</td>
					            <td><a href="afiliados/{{$a->id}}/edit">
					            	<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
					            </a></td>
					        </tr>
					        @endforeach
					    </tbody>
					</table>


				</div>
			</div>
		</div>
	</div>
</div>
@endsection