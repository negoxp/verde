<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Afiliacion PVEM</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!--dynamic table-->
	<link href="{{ asset('/js/advanced-datatable/css/demo_page.css') }}" rel="stylesheet">
	<link href="{{ asset('/js/advanced-datatable/css/demo_table.css') }}" rel="stylesheet">
	<link href="{{ asset('/js/data-tables/DT_bootstrap.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>


	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="{{ asset('images/logo.png') }}" class="img-responsive" style="position: relative; top: -bs-example-navbar-collapse-15px; width:50px;">
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					@if (Auth::check())
					<li><a href="{{ url('/') }}">Inicio</a></li>
					<li><a href="{{ url('/afiliados') }}">Afiliados</a></li>
					@endif
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Entrar</a></li>
						<!--<li><a href="{{ url('/auth/register') }}">Registro</a></li>-->
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<!--dynamic table-->
	<script type="text/javascript" language="javascript" src="{{ asset('') }}js/advanced-datatable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="{{ asset('') }}js/data-tables/DT_bootstrap.js"></script>

	<script type='text/javascript' src="{{ asset('') }}js/dynamic_table_init.js"></script>
	<script type='text/javascript' src="{{ asset('') }}js/jquery.mask.min.js"></script>
	<script type='text/javascript' src="{{ asset('') }}js/jquery.numeric.js"></script>
	<script type='text/javascript' src="{{ asset('') }}js/app.js"></script>
</body>
</html>
