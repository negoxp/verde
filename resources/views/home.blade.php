@extends('app')

@section('content')

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Municipio', 'Afiliados'],
          @foreach($afiliados as $a)
           ['{{$a->municipio}}',  {{$a->total}}],
          @endforeach
        ]);

        var options = {
          title: 'Afiliados por municipio'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Inicio</div>

				<div class="panel-body">
					<div id="piechart" style="width: 900px; height: 500px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
